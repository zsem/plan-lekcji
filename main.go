package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strings"

	"bitbucket.org/zsem/plan-lekcji/config"
	"bitbucket.org/zsem/plan-lekcji/jobs"
	"bitbucket.org/zsem/plan-lekcji/net"
	"bitbucket.org/zsem/plan-lekcji/storage"
	log "github.com/sirupsen/logrus"
)

func main() {
	log.Print("-- Starting Schedule Prettyfier v0.9 --")
	boot()
}

func boot() {
	store := storage.NewJsonStorage(config.Env.Storage)

	iFlagPtr := flag.Bool("i", false, "Interactive mode, asks for commands")
	nFlagPtr := flag.Bool("n", false, "Reload schedules on start")
	flag.Parse()

	if *iFlagPtr {
		runInteractiveModeStartup(store)
	} else {
		runFlagModeStartup(store, *nFlagPtr)
	}

	server := net.NewServer()
	server.DataStorage = store
	go jobs.ScheduleJobs(server)

	server.Boot()
}

func runInteractiveModeStartup(store *storage.JsonStorage) {
	consoleReader := bufio.NewReader(os.Stdin)
	fmt.Print("Do you want to reload schedule from web ? (Y/N)")
	response, _ := consoleReader.ReadString('\n')

	if strings.HasPrefix(strings.ToLower(response), "y") {
		state, err := storage.NewState()

		if err != nil {
			log.WithField("Server", "Boot").Fatal(err.Error())
			os.Exit(1)
		}

		store.StoreAndLoad(state)
	} else {
		store.Load()
	}
}

func runFlagModeStartup(store *storage.JsonStorage, nFlag bool) {
	if nFlag {
		state, err := storage.NewState()

		if err != nil {
			log.WithField("Server", "Boot").Fatal(err.Error())
			os.Exit(1)
		}

		store.StoreAndLoad(state)
	} else {
		store.Load()
	}
}
