const fetchMock = require('fetch-mock')

fetchMock.get('/deputies', JSON.stringify({
  schedules: {
    mo: {
      '4i': {
        1: [
          { lesson: 1, teacher: 'Adam Nowak', class: '4i', group: 0, room: 15 }
        ]
      }
    }
  },
  last_update: 201803170020
}))

fetchMock.get('/state', JSON.stringify({
  timeframes: {
    1: {
      start: '07:10',
      end: '07:55'
    }
  },
  classes: {
    1: [
      { name: '1a automatyk', slug: '1a', schedule: {} }
    ]
  },
  teachers: {
    a: [
      { name: 'K.Adamek', slug: 'kd', schedule: {} }
    ]
  },
  rooms: {
    hades: [
      { name: 'Pracownia UTK', slug: '6', schedule: {} }
    ]
  }
}))

fetchMock.get('/check', '1234')

fetchMock.get('express:/available-rooms/:day/:lesson', JSON.stringify({
  hades: ['5', '6']
}))
