package storage

import (
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/zsem/plan-lekcji/storage/models"
	"bitbucket.org/zsem/plan-lekcji/utils"
)

func (state *State) TeacherBySlug(slug string) (models.Teacher, error) {
	for _, teacherList := range *state.Teachers {
		for _, teacher := range teacherList {
			if strings.Compare(teacher.Slug, slug) == 0 {
				return teacher, nil
			}
		}
	}

	return models.Teacher{}, fmt.Errorf("Teacher with slug %s not found", slug)
}

func (state *State) RoomBySlug(slug string) (models.Room, error) {
	for _, level := range *state.Rooms {
		for _, room := range level {
			if strings.Compare(room.Slug, slug) == 0 {
				return room, nil
			}
		}
	}

	return models.Room{}, fmt.Errorf("Room with slug %s not found", slug)
}

func (state *State) ClassBySlug(slug string) (models.Class, error) {
	classYear, err := strconv.Atoi(slug[:1])

	if err != nil {
		return models.Class{}, fmt.Errorf("Cannot get year from slug string")
	}

	for _, class := range (*state.Classes)[classYear] {

		if strings.Compare(class.Slug, slug) == 0 {
			return class, nil
		}
	}

	return models.Class{}, fmt.Errorf("Class with slug %s not found", slug)
}

func (state *State) ClassReferenceBySlug(slug string) (*models.Class, error) {
	class, err := state.ClassBySlug(slug)
	return &class, err
}

func (state *State) AvailableRooms(day, lessonID int) map[string][]string {
	rooms := []string{}

	for _, level := range *state.Rooms {
		for _, room := range level {
			available := true

			for lesson := range room.Schedule[utils.IndexToDay(day)] {
				if lesson == lessonID {
					available = false
					break
				}
			}

			if available {
				rooms = append(rooms, room.Slug)
			}
		}
	}

	return getCategorizedRooms(rooms)
}

func getCategorizedRooms(rooms []string) map[string][]string {
	categories := map[string][]string{}

	for _, room := range rooms {
		category := utils.CategorizeRoom(room)

		if _, exists := categories[category]; !exists {
			categories[category] = []string{}
		}

		categories[category] = append(categories[category], room)
	}

	return categories
}
