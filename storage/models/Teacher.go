package models

import (
	"bitbucket.org/zsem/plan-lekcji/utils"
)

type Teacher struct {
	Linkable
	Schedule map[string]map[int][]TeacherLesson `json:"schedule"`
}

func NewTeacher(name string, slug string) *Teacher {
	teacher := new(Teacher)

	teacher.Name = name
	teacher.Slug = utils.Slug(slug)
	teacher.Schedule = make(map[string]map[int][]TeacherLesson, 5)

	for i := 0; i <= 4; i++ {
		teacher.Schedule[utils.IndexToDay(i)] = make(map[int][]TeacherLesson)
	}

	return teacher
}

func (teacher Teacher) Cast() Linkable {
	return Linkable{teacher.Name, teacher.Slug}
}
