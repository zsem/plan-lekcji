package scrapping

import (
	"encoding/base64"
	"time"

	"bitbucket.org/zsem/plan-lekcji/config"
	"bitbucket.org/zsem/plan-lekcji/utils"
	"github.com/PuerkitoBio/goquery"
	"github.com/jinzhu/now"
)

type DeputiesScrapper struct {
	Scrapper
}

func NewDeputiesScrapper() *DeputiesScrapper {

	baseScrapper := NewScrapper()

	return &DeputiesScrapper{Scrapper: baseScrapper}
}

func (scrapper *DeputiesScrapper) Scrape() map[string][]*goquery.Document {
	data := make(map[string][]*goquery.Document)
	today := time.Now().Weekday()
	start := now.Monday()

	if today == 0 || today == 6 {
		start = start.AddDate(0, 0, 7)
	}

	for i := 0; i <= 4; i++ {
		date := start.AddDate(0, 0, i)
		slug := utils.IndexToDay(i)
		deputies := scrapper.FetchDeputies(date)
		data[slug] = deputies
	}

	return data
}

func (scrapper *DeputiesScrapper) FetchDeputies(date time.Time) []*goquery.Document {
	link := config.Env.Endpoints.DeputiesUrl + date.Format(config.Env.Deputies.LinkFormat) + ".html"

	scrapper.SetNextRequestHeaders(map[string]string{
		"Authorization": "Basic " + base64.StdEncoding.EncodeToString([]byte(config.Env.Deputies.Name+":"+config.Env.Deputies.Password)),
	})

	doc := scrapper.GetDocument(link)

	return doc
}
