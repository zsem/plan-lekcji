package config

import (
	"encoding/json"
	"io/ioutil"
	"os"

	log "github.com/sirupsen/logrus"
)

type Environment struct {
	Mode      string         `json:"mode"`
	Storage   string         `json:"storage"`
	Lessons   LessonSchedule `json:"lessons"`
	Endpoints Endpoints      `json:"endpoints"`
	Net       Net            `json:"net"`
	Admin     Admin          `json:"admin"`
	Deputies  Deputies       `json:"deputies"`
}

type LessonSchedule struct {
	First        string  `json:"first"`
	Count        int     `json:"count"`
	Length       int     `json:"length"`
	BreakLength  int     `json:"breakLength"`
	CustomBreaks []Break `json:"customBreaks"`
}

type Break struct {
	Start  string `json:"start"`
	Length int    `json:"length"`
}

func LoadEnv() Environment {
	var tree Environment
	contents, err := ioutil.ReadFile("env.json")

	if err != nil {
		log.WithField("Env loader", "Env file parse").Fatal("Couldn't load env file: " + err.Error())
		os.Exit(1)
	}

	json.Unmarshal(contents, &tree)
	validateTree(&tree)

	return tree
}

func validateTree(tree *Environment) {
	checkField(tree.Mode, "", "mode")
	checkField(tree.Storage, "", "storage")
	validateScheduleConfig(&tree.Lessons)
	validateEndpoints(&tree.Endpoints)
	validateNet(&tree.Net)
	validateAdmin(&tree.Admin)
	validateDeputies(&tree.Deputies)
}

func validateScheduleConfig(schedule *LessonSchedule) {
	checkField(schedule.Count, 0, "lessons|count")
	checkField(schedule.First, "", "lessons|first")
	checkField(schedule.BreakLength, 0, "lessons|breakLength")
	checkField(schedule.Length, 0, "lessons|length")
}

func checkField(val interface{}, nill interface{}, field string) {
	if val == nill {
		log.WithField("Env loader", "Env validation").Fatalf("Env key %s was not found in env file", field)
		os.Exit(1)
	}
}
