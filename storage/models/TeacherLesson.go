package models

type TeacherLesson struct {
	Lesson
	Class string `json:"class"`
	Room  string `json:"room"`
}

func NewTeacherLesson(name string, class string, room string) TeacherLesson {
	return TeacherLesson{Lesson{name}, class, room}
}
