export default {
  ready: false,
  error: false,
  updating: false,
  timeframes: {},
  classes: {},
  teachers: {},
  classrooms: {},
  header: {
    title: undefined,
    subtitle: undefined
  },
  actionBarVisible: true,
  availableRooms: {},
  availableRoomsLoading: false,
  deputies: null,
  favourite: {
    type: '',
    slug: ''
  }
}
