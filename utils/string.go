package utils

import (
	"strings"
	"unicode"
)

func ReplacePolishCharacters(str string) string {
	replacements := []string{"ó", "o", "ą", "a", "ć", "c", "ę", "e", "ż", "z", "ź", "z", "ł", "l", "ń", "n", "ś", "s", "ł", "l"}
	replacer := strings.NewReplacer(replacements...)

	return replacer.Replace(str)
}

func IsEmpty(str string) bool {
	stripped := strings.Map(func(r rune) rune {
		if unicode.IsSpace(r) {
			return -1
		}
		return r
	}, str)

	return len(stripped) == 0
}

func SolidToCamelCase(str string) string {
	runes := []rune(str)
	runes[0] = unicode.ToLower(runes[0])

	return string(runes)
}

func Slug(str string) string {
	return strings.ToLower(strings.Replace(str, " ", "", -1))
}
