package scrapping

import (
	"bitbucket.org/zsem/plan-lekcji/config"
	"bitbucket.org/zsem/plan-lekcji/utils"
	"github.com/PuerkitoBio/goquery"
)

type TeacherScrapper struct {
	Scrapper
}

func NewTeacherScrapper() *TeacherScrapper {

	baseScrapper := NewScrapper()

	return &TeacherScrapper{Scrapper: baseScrapper}
}

func (scrapper TeacherScrapper) Scrape() []*goquery.Document {
	links := scrapper.GetDocument(config.Env.Endpoints.RootUrl + config.Env.Endpoints.TeacherUrl)
	filtered := utils.RetrieveLinksWithPrefix(links, "plany/n")
	schedules := scrapper.FetchDocuments(filtered)

	return schedules
}
