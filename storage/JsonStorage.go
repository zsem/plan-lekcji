package storage

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
)

type JsonStorage struct {
	*State
	StoragePath        string
	StoragePermissions os.FileMode
}

func NewJsonStorage(storagePath string) *JsonStorage {
	storage := new(JsonStorage)
	storage.State = &State{}
	storage.StoragePath = storagePath
	storage.StoragePermissions = 0777

	return storage
}

func (storage *JsonStorage) Load() {
	jison, err := ioutil.ReadFile(storage.StoragePath)

	if err != nil {
		panic(err)
	}

	json.Unmarshal(jison, storage.State)
}

func (storage *JsonStorage) Store(state *State) error {
	encoded, err := json.Marshal(state)

	if err != nil {
		return err
	}

	err = ioutil.WriteFile(storage.StoragePath, encoded, storage.StoragePermissions)

	return err
}

func (storage *JsonStorage) StoreAndLoad(state *State) {
	jison, oldStoreErr := ioutil.ReadFile(storage.StoragePath)

	if oldStoreErr != nil && !os.IsNotExist(oldStoreErr) {
		log.WithField("Storage", "Backup creation").Fatalf("Cannot backup old storage: %v", oldStoreErr)
		os.Exit(1)
	}

	backupPath := strings.Replace(storage.StoragePath, ".json", "_old.json", 1)
	_, backupReadErr := ioutil.ReadFile(backupPath)

	if backupReadErr != nil && os.IsNotExist(backupReadErr) {
		file, err := os.Create(backupPath)

		if err != nil {
			log.WithField("Storage", "Backup creation").Fatalf("Cannot create backup file in advance: %v", err)
			os.Exit(1)
		}

		file.Close()
	}

	backupWriteErr := ioutil.WriteFile(backupPath, jison, storage.StoragePermissions)

	if backupWriteErr != nil {
		log.WithField("Storage", "Backup creation").Fatalf("Cannot backup old storage: %v", backupWriteErr)
		os.Exit(1)
	}

	storage.State = state

	writeErr := storage.Store(state)

	if writeErr != nil {
		log.WithField("Storage", "State storage").Fatalf("Cannot store new state: %v", writeErr)
		os.Exit(1)
	}
}

func (storage *JsonStorage) GetState() *State {
	return storage.State
}
