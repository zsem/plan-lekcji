import * as getters from '@/store/getters'

const state = {
  ready: true,
  error: true,
  updating: true,
  timeframes: {
    1: {
      start: '07:10',
      end: '07:55'
    },
    2: {
      start: '08:00',
      end: '08:45'
    }
  },
  classes: {
    1: [],
    2: [],
    3: [],
    4: []
  },
  teachers: {
    a: [],
    b: [],
    c: []
  },
  classrooms: {
    first: [],
    second: []
  },
  header: {
    title: 'Plan lekcji',
    subtitle: '4i technik informatyk'
  },
  actionBarVisible: true,
  availableRooms: {
    first: [],
    second: []
  },
  availableRoomsLoading: false,
  favourite: {
    type: 'clazz',
    slug: '4i'
  },
  deputies: {
    schedules: {
      mo: {
        '4i': {
          1: [
            { lesson: 1, teacher: 'Adam Nowak', class: '4i', group: 0, room: 15 }
          ]
        }
      }
    },
    last_update: 201803170020
  }
}

describe('getters', () => {
  it('isReady', () => {
    expect(getters.isReady(state)).to.equal(true)
  })

  it('isUpdating', () => {
    expect(getters.isUpdating(state)).to.equal(true)
  })

  it('hasError', () => {
    expect(getters.hasError(state)).to.equal(true)
  })

  it('getDeputies', () => {
    expect(getters.getDeputies(state)).to.deep.equal({
      schedules: {
        mo: {
          '4i': {
            1: [
              { lesson: 1, teacher: 'Adam Nowak', class: '4i', group: 0, room: 15 }
            ]
          }
        }
      },
      last_update: 201803170020
    })
  })

  it('getTimeframes', () => {
    expect(getters.getTimeframes(state)).to.deep.equal({
      1: {
        start: '07:10',
        end: '07:55'
      },
      2: {
        start: '08:00',
        end: '08:45'
      }
    })
  })

  it('getClassesList', () => {
    expect(getters.getClassesList(state)).to.deep.equal({
      1: [],
      2: [],
      3: [],
      4: []
    })
  })

  it('getTeachersList', () => {
    expect(getters.getTeachersList(state)).to.deep.equal({
      a: [],
      b: [],
      c: []
    })
  })

  it('getClassroomsList', () => {
    expect(getters.getClassroomsList(state)).to.deep.equal({
      first: [],
      second: []
    })
  })

  it('getHeaderTitle', () => {
    expect(getters.getHeaderTitle(state)).to.equal('Plan lekcji')
  })

  it('getHeaderSubtitle', () => {
    expect(getters.getHeaderSubtitle(state)).to.equal('4i technik informatyk')
  })

  it('isActionBarVisible', () => {
    expect(getters.isActionBarVisible(state)).to.equal(true)
  })

  it('getAvailableRooms', () => {
    expect(getters.getAvailableRooms(state)).to.deep.equal({
      first: [],
      second: []
    })
  })

  it('areAvailableRoomsLoading', () => {
    expect(getters.areAvailableRoomsLoading(state)).to.equal(false)
  })

  it('getFavouriteSchedule', () => {
    expect(getters.getFavouriteSchedule(state)).to.deep.equal({
      type: 'clazz',
      slug: '4i'
    })
  })
})
