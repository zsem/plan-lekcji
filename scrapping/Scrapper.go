package scrapping

import (
	"net/http"
	"strings"
	"sync"

	"bitbucket.org/zsem/plan-lekcji/config"
	"github.com/PuerkitoBio/goquery"
	log "github.com/sirupsen/logrus"
)

type Scrapper struct {
	ChannelSize    int
	HttpClient     *http.Client
	RequestHeaders map[string]string
}

func NewScrapper() Scrapper {
	return Scrapper{
		ChannelSize:    20,
		HttpClient:     &http.Client{},
		RequestHeaders: make(map[string]string),
	}
}

func (scrapper *Scrapper) Scrape() {

}

func (scrapper *Scrapper) FetchDocuments(links []*goquery.Selection) []*goquery.Document {
	var docs []*goquery.Document
	wg := sync.WaitGroup{}
	ch := make(chan *goquery.Document, 100)
	for _, link := range links {
		wg.Add(1)
		go func(link *goquery.Selection) {
			defer wg.Done()

			href, exists := link.Attr("href")

			if !exists {
				return
			}

			doc := scrapper.GetDocument(config.Env.Endpoints.RootUrl + href)

			ch <- doc[0]
		}(link)
	}

	wg.Wait()
	close(ch)

	for doc := range ch {
		docs = append(docs, doc)
	}

	return docs
}

func (scrapper *Scrapper) GetDocument(url string) []*goquery.Document {
	nodes := []*goquery.Document{}
	channel := make(chan *goquery.Document, scrapper.ChannelSize)
	group := &sync.WaitGroup{}

	group.Add(1)
	go scrapper.Collect(url, channel, group)

	group.Wait()
	close(channel)

	for node := range channel {
		nodes = append(nodes, node)
	}

	return nodes
}

func (scrapper *Scrapper) Collect(url string, channel chan *goquery.Document, group *sync.WaitGroup) {
	defer group.Done()
	res, err := scrapper.fetchResource(url)

	if err != nil {
		log.WithField("Base Scrapper", "Resource fetch").Warn("Failed to fetch resource with url" + url + ", skipping Got error: ")
		return
	}

	rootDoc, err := goquery.NewDocumentFromResponse(res)

	if err != nil {
		log.WithField("Base Scrapper", "Resource parse").Warn("Failed to parse resource with url" + url + ", skipping. Got error: " + err.Error())
		return
	}

	channel <- rootDoc

	frameSources := scrapper.RetrieveFrameSources(rootDoc)

	for _, source := range frameSources {
		group.Add(1)
		go scrapper.Collect(source, channel, group)
	}

	return
}

func (scrapper *Scrapper) RetrieveFrameSources(document *goquery.Document) []string {
	sources := []string{}

	document.Find("frame").Each(func(index int, node *goquery.Selection) {
		src, exists := node.Attr("src")

		if !exists {
			return
		}

		if strings.HasPrefix(src, "http") {
			sources = append(sources, src)
		} else {
			sources = append(sources, config.Env.Endpoints.RootUrl+src)
		}
	})

	return sources
}

func (scrapper *Scrapper) SetNextRequestHeaders(headers map[string]string) {
	for header, value := range headers {
		scrapper.RequestHeaders[header] = value
	}
}

func (scrapper *Scrapper) fetchResource(url string) (*http.Response, error) {
	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		return &http.Response{}, err
	}

	for header, value := range scrapper.RequestHeaders {
		req.Header.Add(header, value)
	}

	res, err := scrapper.HttpClient.Do(req)
	scrapper.RequestHeaders = make(map[string]string)

	return res, err
}
