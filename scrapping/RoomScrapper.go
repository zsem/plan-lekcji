package scrapping

import (
	"bitbucket.org/zsem/plan-lekcji/config"
	"bitbucket.org/zsem/plan-lekcji/utils"
	"github.com/PuerkitoBio/goquery"
)

type RoomScrapper struct {
	Scrapper
}

func NewRoomScrapper() *RoomScrapper {

	baseScrapper := NewScrapper()

	return &RoomScrapper{Scrapper: baseScrapper}
}

func (scrapper RoomScrapper) Scrape() []*goquery.Document {
	links := scrapper.GetDocument(config.Env.Endpoints.RootUrl + config.Env.Endpoints.RoomUrl)
	filtered := utils.RetrieveLinksWithPrefix(links, "plany/s")
	schedules := scrapper.FetchDocuments(filtered)

	return schedules
}
