Przy uruchamianiu planera z poziomu konsoli, mamy możliwość użycia flag aplikacji, co pozwala na automatyzację startu programu w przypadku użycia metod zdalnych. Każda opcja ma format "-flaga" i podawana jest zaraz po nazwie pliku wykonywalnego planera np. "plan.exe -n". Dostępne aktualnie flagi to:

* *i* - tryb interaktwny, prowadzi krok po kroku przez start planu, domyślnie wyłączony

* *n* - zmusza aplikację do pobrania najnowszego planu przy rozruchu, ignorując poprzednio zarchiwizowane wersje

Oprócz flag, za konfigurację aplikacji odpowiada plik "env.json" w katalogu głównym planera, definiujący najważniejsze zmienne aplikacji, takie jak adresy stron z danymi, czy dane dostępowe do opcji aktualizacji planu. Wartości domyślne zdefiniowane są w dostarczonym pliku, w przydaku braku jakiejkolwiek wymaganej wartości, aplikacja będzie domagać się jej uzupełnienia przy rozruchu, i nie rozpocznie poprawnie pracy. 