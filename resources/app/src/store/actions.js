import * as api from '@/api'

const FAV_KEY = 'fav'
const STORAGE_KEY = 'data'

export const getData = ({ commit, dispatch }) => {
  dispatch('getFavouriteSchedule')
  dispatch('fetchDeputies')

  if (localStorage.getItem(STORAGE_KEY) !== null) {
    dispatch('setData', JSON.parse(localStorage.getItem('data')))
    dispatch('checkForUpdate')
    return
  }

  dispatch('fetchData')
}

export const getFavouriteSchedule = ({ commit }) => {
  const fav = localStorage.getItem(FAV_KEY) || '{}'
  commit('setFavouriteSchedule', JSON.parse(fav))
}

export const fetchData = ({ commit, dispatch }) => {
  commit('setUpdating', true)

  api.getData()
    .then(result => {
      dispatch('setData', result)
      dispatch('saveData', result)
      commit('setUpdating', false)
    })
    .catch(error => {
      commit('setError', true)
      commit('setUpdating', false)
      console.error('Error while fetching data from server', error)
    })
}

export const fetchDeputies = ({ commit }) => {
  if (!navigator.onLine) {
    commit('setDeputies', {})
    return
  }

  api.getDeputies()
    .then(result => {
      commit('setDeputies', result)
    })
    .catch(error => {
      console.log(error)
      commit('setDeputies', {})
    })
}

export const setData = ({ commit }, data) => {
  commit('setData', data)
  commit('setReady', true)
}

export const saveData = ({ commit }, data) => {
  localStorage.setItem('data', JSON.stringify(data))
}

export const checkForUpdate = ({ dispatch, state }) => {
  if (!navigator.onLine) {
    return
  }

  api.getVersionHash()
    .then(hash => {
      if (hash !== state.last_update) {
        dispatch('updateData')
      }
    })
    .catch(() => {})
}

export const updateData = ({ dispatch, commit }) => {
  localStorage.removeItem(STORAGE_KEY)
  dispatch('getData')
}

export const setHeader = ({ commit }, payload) => {
  commit('setHeader', payload)
}

export const clearHeader = ({ dispatch }) => {
  dispatch('setHeader', {})
}

export const setActionBarVisible = ({ commit }, visible) => {
  commit('setActionBarVisible', visible)
}

export const getAvailableRooms = ({ commit }, query) => {
  if (!navigator.onLine) {
    return
  }

  commit('setAvailableRoomsLoading', true)
  api.getAvailableRooms(query)
    .then(rooms => {
      commit('setAvailableRooms', rooms)
      commit('setAvailableRoomsLoading', false)
    })
    .catch(() => {})
}

export const setFavouriteSchedule = ({ commit }, payload = {}) => {
  commit('setFavouriteSchedule', payload)
  window.localStorage.setItem(FAV_KEY, JSON.stringify(payload))
}
