package utils

import (
	"github.com/PuerkitoBio/goquery"
)

func SameNodeCount(slices ...*goquery.Selection) bool {
	for key := range slices {

		if key+1 == len(slices) {
			break
		}

		if slices[key].Length() != slices[key+1].Length() {
			return false
		}
	}

	return true
}
