package models

type TeacherSchedule struct {
	TeacherLesson
	Timestamp
}

func NewTeacherSchedule(lesson TeacherLesson, start string, end string) TeacherSchedule {
	return TeacherSchedule{lesson, Timestamp{start, end}}
}
