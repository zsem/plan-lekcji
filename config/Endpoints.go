package config

type Endpoints struct {
	RootUrl     string `json:"rootUrl"`
	DeputiesUrl string `json:"deputiesUrl"`
	ScheduleUrl string `json:"scheduleUrl"`
	TeacherUrl  string `json:"teacherUrl"`
	RoomUrl     string `json:"roomUrl"`
}

func validateEndpoints(endpoints *Endpoints) {
	checkField(endpoints.RootUrl, "", "endpoints|rootUrl")
	checkField(endpoints.DeputiesUrl, "", "endpoints|deputiesUrl")
	checkField(endpoints.ScheduleUrl, "", "endpoints|scheduleUrl")
	checkField(endpoints.TeacherUrl, "", "endpoints|teacherUrl")
	checkField(endpoints.RootUrl, "", "endpoints|roomUrl")
}
