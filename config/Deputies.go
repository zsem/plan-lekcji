package config

type Deputies struct {
	Name       string `json:"name"`
	Password   string `json:"password"`
	LinkFormat string `json:"linkFormat"`
}

func validateDeputies(deputies *Deputies) {
	checkField(deputies.Name, "", "deputies|name")
	checkField(deputies.Password, "", "deputies|password")
	checkField(deputies.LinkFormat, "", "deputies|linkFormat")
}
