package interfaces

import "bitbucket.org/zsem/plan-lekcji/storage/models"

type Linker interface {
	CastToLinks() []models.Linkable
}
