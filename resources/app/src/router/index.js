import Vue from 'vue'
import Router from 'vue-router'

import Search from '@/pages/search/Search'
import Classes from '@/pages/classes/Classes'
import Teachers from '@/pages/teachers/Teachers'
import Classrooms from '@/pages/classrooms/Classrooms'
import Schedule from '@/pages/schedule/Schedule'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: '/',
  routes: [
    {
      path: '*',
      redirect: '/klasy'
    },
    {
      path: '/klasy',
      name: 'classes',
      component: Classes,
      meta: {
        header: {
          title: 'Lista klas',
          background: 'classes.jpg'
        }
      }
    },
    {
      path: '/klasy/:clazz',
      name: 'clazz',
      component: Schedule,
      meta: {
        header: {
          background: 'classes.jpg'
        }
      }
    },
    {
      path: '/nauczyciele',
      name: 'teachers',
      component: Teachers,
      meta: {
        header: {
          title: 'Lista nauczycieli',
          background: 'teachers.jpg'
        }
      }
    },
    {
      path: '/nauczyciele/:teacher',
      name: 'teacher',
      component: Schedule,
      meta: {
        header: {
          background: 'teachers.jpg'
        }
      }
    },
    {
      path: '/sale',
      name: 'classrooms',
      component: Classrooms,
      meta: {
        header: {
          title: 'Lista sal',
          background: 'classrooms.jpg'
        }
      }
    },
    {
      path: '/sale/:classroom',
      name: 'classroom',
      component: Schedule,
      meta: {
        header: {
          background: 'classrooms.jpg'
        }
      }
    },
    {
      path: '/wolne-sale',
      name: 'search',
      component: Search,
      meta: {
        header: {
          title: 'Wolne sale',
          background: 'search.jpg'
        }
      }
    },
    {
      path: '/schedule',
      name: 'schedule',
      component: Schedule,
      meta: {
        header: {
          title: 'Podział godzin',
          background: 'search.jpg'
        }
      }
    }
  ]
})
