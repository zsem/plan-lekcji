package jobs

import (
	"bitbucket.org/zsem/plan-lekcji/net"
	"github.com/jasonlvhit/gocron"
)

func ScheduleJobs(server *net.Server) {
	gocron.Every(1).Hours().Do(UpdateDeputies(server.DataStorage.GetState()))
	gocron.RunAll()
	<-gocron.Start()
}
