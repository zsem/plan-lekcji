package net

import (
	"encoding/json"
	"net/http"
)

func JsonResponse(w http.ResponseWriter, data interface{}) {
	jison, err := json.Marshal(data)

	if err != nil {
		http.Error(w, "Data conversion Error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(jison)
}
