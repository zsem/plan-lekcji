package parsing

import (
	"os"

	"bitbucket.org/zsem/plan-lekcji/config"
	"bitbucket.org/zsem/plan-lekcji/storage/models"
	"bitbucket.org/zsem/plan-lekcji/utils"
	log "github.com/sirupsen/logrus"
)

type LessonTimeframeParser struct {
}

func NewLessonTimeframeParser() *LessonTimeframeParser {
	return &LessonTimeframeParser{}
}

func (parser *LessonTimeframeParser) PrepareData() *map[int]models.LessonTimeframe {
	timeframes := make(map[int]models.LessonTimeframe)
	cfg := config.Env.Lessons
	clock, err := utils.ClockFromString(cfg.First)

	if err != nil {
		log.WithField("Lesson Parsing", "Timeframe generation").Fatal("Could not create clock from string. Got error: " + err.Error())
		os.Exit(1)
	}

	for count := 1; count <= cfg.Count; count++ {
		frame := models.NewLessonTimeframe()
		frame.Start = clock.Time()
		clock.Add(cfg.Length)
		frame.End = clock.Time()

		customBreak := false

		for _, lesson := range cfg.CustomBreaks {
			if lesson.Start == clock.Time() {
				clock.Add(lesson.Length)
				customBreak = true
				break
			}
		}

		if !customBreak {
			clock.Add(cfg.BreakLength)
		}

		timeframes[count] = frame
		customBreak = false
	}

	log.WithField("Lesson Parsing", "Parse End").Infof("Generated %d lesson schedules", len(timeframes))
	return &timeframes
}
