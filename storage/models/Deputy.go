package models

type Deputy struct {
	Lesson  int    `json:"-"`
	Teacher string `json:"teacher"`
	Class   string `json:"-"`
	Group   string `json:"group"`
	Room    string `json:"room"`
}
