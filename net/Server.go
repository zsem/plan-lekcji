package net

import (
	"net/http"
	"os"

	"bitbucket.org/zsem/plan-lekcji/config"
	"bitbucket.org/zsem/plan-lekcji/storage"
	"github.com/CloudyKit/jet"
	"github.com/go-zoo/bone"
	"github.com/go-zoo/claw"
	log "github.com/sirupsen/logrus"
)

type Server struct {
	Router                *Router
	HandledRequest        *http.Request
	TplEngine             *jet.Set
	DataStorage           *storage.JsonStorage
	HandledResponseWriter http.ResponseWriter
}

func NewServer() *Server {
	server := new(Server)
	server.Router = NewRouter(server)
	server.TplEngine = jet.NewHTMLSet(config.Env.Net.ViewPath)

	if config.Env.Mode == "dev" {
		server.TplEngine.SetDevelopmentMode(true)
	}

	return server
}

func (server *Server) Boot() {
	RegisterRoutes(server.Router)
	log.WithField("Server", "Boot").Info("Starting server on port " + config.Env.Net.ServerPort)
	log.Fatal(http.ListenAndServe(":"+config.Env.Net.ServerPort, server.Router.Handler()))
}

func (server *Server) TryFile(notFoundHandler RouterHandler) http.Handler {
	return server.CreateRequestHandler(func(sv *Server) {
		req := server.HandledRequest
		file, err := os.Stat(config.Env.Net.PublicPath + req.URL.Path)

		if !os.IsNotExist(err) && file.Mode().IsRegular() {
			http.ServeFile(server.HandledResponseWriter, req, config.Env.Net.PublicPath+req.URL.Path)
		} else {
			notFoundHandler(server)
		}
	})
}

func (server *Server) CreateAuthorizedHandler(handler RouterHandler) http.Handler {
	return server.CreateRequestHandler(func(sv *Server) {
		r := server.HandledRequest

		username := r.FormValue("username")
		password := r.FormValue("password")

		if username != config.Env.Admin.Name || password != config.Env.Admin.Password {
			server.Error(401, "Wrong credentials")
			return
		}

		handler(sv)
	})
}

func (server *Server) CreateRequestHandler(handler RouterHandler) http.Handler {
	stack := claw.New(Cors)

	return stack.Use(func(w http.ResponseWriter, req *http.Request) {
		server.HandledRequest = req
		server.HandledResponseWriter = w

		handler(server)
	})
}

func (server *Server) View(path string, vars map[string]interface{}) {
	t, err := server.TplEngine.GetTemplate(path + ".html")
	varmap := make(jet.VarMap)

	if err != nil {
		if config.Env.Mode == "prod" {
			server.Error(500, "Nastąpił błąd serwera, przepraszamy :( ")
		} else {
			server.Error(500, "Couldn't parse template: \n"+err.Error())
		}

		return
	}

	if vars != nil {
		for key, value := range vars {
			varmap.Set(key, value)
		}
	}

	server.HandledResponseWriter.Header().Set("Content-Type", "text/html")

	if err := t.Execute(server.HandledResponseWriter, varmap, nil); err != nil {
		server.HandledResponseWriter.Header().Set("Content-Type", "application/json")
		if config.Env.Mode == "prod" {
			server.Error(500, "Nastąpił błąd serwera, przepraszamy :( ")
		} else {
			server.Error(500, "Couldn't compile template: \n"+err.Error())
		}
	}

}

func (server *Server) Param(param string) string {
	return bone.GetValue(server.HandledRequest, param)
}
func (server *Server) Json(data interface{}) {
	JsonResponse(server.HandledResponseWriter, data)
}

func (server *Server) Error(code int, message string) {
	http.Error(server.HandledResponseWriter, message, code)
}
