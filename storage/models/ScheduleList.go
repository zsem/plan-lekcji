package models

type ScheduleList struct {
	Teachers []PanelLink `json:"teachers"`
	Rooms    []PanelLink `json:"rooms"`
	Classes  []PanelLink `json:"classes"`
}
