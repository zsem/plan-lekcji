package models

type ClassLesson struct {
	Name    string `json:"name"`
	Teacher string `json:"teacher"`
	Room    string `json:"room"`
}

func NewClassLesson(name string, teacher string, room string) ClassLesson {
	return ClassLesson{name, teacher, room}
}
