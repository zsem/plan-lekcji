package models

import "bitbucket.org/zsem/plan-lekcji/utils"

type Room struct {
	Name     string                          `json:"name"`
	Slug     string                          `json:"slug"`
	Schedule map[string]map[int][]RoomLesson `json:"schedule"`
}

func NewRoom(name string, slug string) *Room {
	room := new(Room)

	room.Name = name
	room.Slug = slug
	room.Schedule = make(map[string]map[int][]RoomLesson, 5)

	for i := 0; i <= 4; i++ {
		room.Schedule[utils.IndexToDay(i)] = make(map[int][]RoomLesson)
	}

	return room
}

func (room Room) Cast() Linkable {
	return Linkable{room.Name, room.Slug}
}
