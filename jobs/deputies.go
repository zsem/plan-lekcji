package jobs

import (
	"bitbucket.org/zsem/plan-lekcji/parsing"
	"bitbucket.org/zsem/plan-lekcji/storage"
	log "github.com/sirupsen/logrus"
)

func UpdateDeputies(state *storage.State) func() {
	return func() {
		deputies := parsing.NewDeputiesParser().PrepareData()
		state.Deputies = deputies
		log.WithField("Server", "Scheduler").Info("Deputies updated successfully")
	}
}
