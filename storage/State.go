package storage

import (
	"bitbucket.org/zsem/plan-lekcji/parsing"
	"bitbucket.org/zsem/plan-lekcji/storage/models"
	"bitbucket.org/zsem/plan-lekcji/utils"
	log "github.com/sirupsen/logrus"
)

type State struct {
	Timeframes *map[int]models.LessonTimeframe `json:"timeframes"`
	Classes    *map[int]models.ClassList       `json:"classes"`
	Teachers   *models.TeacherList             `json:"teachers"`
	Rooms      *models.RoomList                `json:"rooms"`
	Deputies   *models.Deputies                `json:"-"`
	LastUpdate int                             `json:"last_update"`
}

func NewState() (*State, error) {
	state := new(State)
	state.Timeframes = parsing.NewLessonTimeframeParser().PrepareData()
	state.Classes = parsing.NewClassParser().PrepareData()
	state.Teachers = parsing.NewTeacherParser().PrepareData()
	state.Rooms = parsing.NewRoomParser().PrepareData()

	timestamp, err := utils.CurrentDateTimestamp()

	if err != nil {
		log.WithField("State", "Timestamp").Fatal("Timestamp generation for state failed: " + err.Error())
		state.LastUpdate = 0
	} else {
		state.LastUpdate = timestamp
	}

	return state, err
}
