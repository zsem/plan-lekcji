package models

import "bitbucket.org/zsem/plan-lekcji/utils"

type Class struct {
	Name      string                           `json:"name"`
	Slug      string                           `json:"slug"`
	Schedules map[string]map[int][]ClassLesson `json:"schedule"`
}

func (class *Class) InitScheduleMap() {
	schedules := map[string]map[int][]ClassLesson{}

	for i := 0; i <= 4; i++ {
		schedules[utils.IndexToDay(i)] = make(map[int][]ClassLesson)
	}

	class.Schedules = schedules
}

func (class Class) Cast() Linkable {
	return Linkable{class.Name, class.Slug}
}
