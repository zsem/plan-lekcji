package utils

import (
	"bytes"
	"github.com/PuerkitoBio/goquery"
	"golang.org/x/net/html"
	"strings"
)

func RetrieveLinksWithPrefix(docs []*goquery.Document, prefix string) []*goquery.Selection {
	links := []*goquery.Selection{}

	for _, doc := range docs {
		doc.Find("a[href]").Each(func(i int, selection *goquery.Selection) {
			attr, exists := selection.Attr("href")

			if exists && strings.HasPrefix(attr, prefix) {
				links = append(links, selection)
			}
		})
	}

	return links
}

func NodeTextContent(n *html.Node) string {
	textBuffer := &bytes.Buffer{}

	collectText(n, textBuffer)

	return textBuffer.String()
}

func collectText(n *html.Node, buffer *bytes.Buffer) {
	if n.Type == html.TextNode {
		buffer.WriteString(n.Data)
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		collectText(c, buffer)
	}
}
