package utils

func Between(start, end, num int) bool {
	return num >= start && num <= end
}
