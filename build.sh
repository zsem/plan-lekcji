go get
go build -o bin/plan.exe
sudo chmod 770 bin/plan.exe
cp -p "env_production.json" "bin/env.json"
cd resources/app
npm install
npm run build
cp -r -p ./dist ../../bin/app
