package parsing

import (
	"strconv"
	"strings"
	"unicode"

	"bitbucket.org/zsem/plan-lekcji/scrapping"
	"bitbucket.org/zsem/plan-lekcji/storage/models"
	"bitbucket.org/zsem/plan-lekcji/utils"
	"github.com/PuerkitoBio/goquery"
	log "github.com/sirupsen/logrus"
)

type TeacherParser struct {
	scrapper *scrapping.TeacherScrapper
}

func NewTeacherParser() *TeacherParser {
	return &TeacherParser{scrapping.NewTeacherScrapper()}
}

func (parser *TeacherParser) PrepareData() *models.TeacherList {

	teacherList := models.TeacherList{}
	schedules := parser.scrapper.Scrape()

	for _, schedule := range schedules {
		teacher := parseTeacherSchedule(schedule)
		firstLetter := surnameFirstLetter(teacher.Name)

		teacherList[firstLetter] = append(teacherList[firstLetter], teacher)
	}

	log.WithField("Teacher Parsing", "Parse End").Infof("Generated %d teacher schedules", len(teacherList))
	return &teacherList
}

func parseTeacherSchedule(schedule *goquery.Document) models.Teacher {
	replacer := strings.NewReplacer("(", "", ")", "")
	teacherTitle := schedule.Find(".tytulnapis").Text()
	teacherData := strings.Split(replacer.Replace(teacherTitle), " ")
	teacher := models.NewTeacher(teacherData[0], teacherData[1])
	schedule.Find(".tabela > tbody > tr").Each(func(i int, lesson *goquery.Selection) {
		parseTeacherLesson(lesson, teacher)
	})

	return *teacher
}

func parseTeacherLesson(lesson *goquery.Selection, teacher *models.Teacher) {
	lessonId, err := strconv.Atoi(lesson.Find(".nr").Text())
	days := lesson.Find(".l")

	if err != nil {
		return
	}

	days.Each(func(index int, cell *goquery.Selection) {
		text := cell.Text()
		if !utils.IsEmpty(text) {
			teacher.Schedule[utils.IndexToDay(index)][lessonId] = []models.TeacherLesson{parseTeacherCell(cell)}
		}
	})
}

func parseTeacherCell(cell *goquery.Selection) models.TeacherLesson {

	class := cell.Find(".o")
	lessonName := cell.Find(".p")
	room := cell.Find(".s")

	// Parse błędnie sformatowanej komórki
	if class.Length() == 0 {
		return models.NewTeacherLesson(cell.Text(), "", "")
	}

	return models.NewTeacherLesson(lessonName.Text(), utils.Slug(class.Text()[:2]), room.Text())
}

func surnameFirstLetter(teacherName string) string {
	return string(unicode.ToLower(rune(strings.Split(teacherName, ".")[1][0])))
}
