package scrapping

import (
	"bitbucket.org/zsem/plan-lekcji/config"
	"bitbucket.org/zsem/plan-lekcji/utils"
	"github.com/PuerkitoBio/goquery"
)

type ClassScrapper struct {
	Scrapper
}

func NewClassScrapper() *ClassScrapper {
	baseScrapper := NewScrapper()

	return &ClassScrapper{Scrapper: baseScrapper}
}

func (scrapper ClassScrapper) Scrape() []*goquery.Document {
	links := scrapper.GetDocument(config.Env.Endpoints.RootUrl + config.Env.Endpoints.ScheduleUrl)
	filtered := utils.RetrieveLinksWithPrefix(links, "plany/o")
	schedules := scrapper.FetchDocuments(filtered)

	return schedules
}
