package parsing

import (
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"

	"bitbucket.org/zsem/plan-lekcji/scrapping"
	"bitbucket.org/zsem/plan-lekcji/storage/models"
	"bitbucket.org/zsem/plan-lekcji/utils"
	"github.com/PuerkitoBio/goquery"
)

type ClassParser struct {
	scrapper *scrapping.ClassScrapper
}

func NewClassParser() *ClassParser {
	return &ClassParser{scrapping.NewClassScrapper()}
}

func (parser *ClassParser) PrepareData() *map[int]models.ClassList {
	classList := make(map[int]models.ClassList)
	schedules := parser.scrapper.Scrape()
	count := 0

	for _, schedule := range schedules {
		class := parseSchedule(schedule)
		classYear, yearErr := strconv.Atoi(class.Slug[:1])

		if yearErr != nil {
			log.WithField("Class Parsing", "Slug extraction").Warnf("Failed to extract year from class slug", class.Slug)
			continue
		}

		count++
		classList[classYear] = append(classList[classYear], class)
	}

	log.WithField("Class Parsing", "Parse End").Infof("Generated %d class schedules", count)
	return &classList
}

func parseSchedule(schedule *goquery.Document) models.Class {
	class := models.Class{}
	className := schedule.Find(".tytulnapis").Text()
	class.Name = className
	class.Slug = utils.Slug(className[:2])
	class.InitScheduleMap()

	schedule.Find(".tabela > tbody > tr").Each(func(i int, lesson *goquery.Selection) {
		parseScheduleLesson(lesson, &class)
	})

	return class
}

func parseScheduleLesson(lesson *goquery.Selection, class *models.Class) {
	lessonText := lesson.Find(".nr").Text()
	lessonID, err := strconv.Atoi(lessonText)
	days := lesson.Find(".l")

	if err != nil && lessonText != "" {
		log.WithField("Schedule Parser", "Schedule Lesson Parse").Warnf("Error while parsing lessonID to int: %s", err.Error())
		return
	}

	days.Each(func(index int, cell *goquery.Selection) {
		text := cell.Text()
		if !utils.IsEmpty(text) {
			class.Schedules[utils.IndexToDay(index)][lessonID] = parseCell(cell)
		}
	})
}

func parseCell(cell *goquery.Selection) []models.ClassLesson {
	var lessons = []models.ClassLesson{}

	elements := cell.Find(".p, .n, .s")

	if elements.Length() != 0 {
		for i, j := 0, elements.Length(); i < j; i += 3 {
			lesson := utils.NodeTextContent(elements.Get(i))
			teacher := utils.Slug(utils.NodeTextContent(elements.Get(i + 1)))
			room := utils.NodeTextContent(elements.Get(i + 2))

			// Język francuski - błędny nauczyciel
			if strings.HasPrefix(teacher, "#jf") {
				teacher = "ds"
			}

			lessons = append(lessons, models.NewClassLesson(lesson, teacher, room))
		}

		return lessons
	}

	// PARSE CZYSTEGO TEKSTU
	lessons = append(lessons, models.NewClassLesson(cell.Text(), "", ""))
	return lessons
}
