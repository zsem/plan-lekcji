package utils

import (
	log "github.com/sirupsen/logrus"
)

func Signal(from string, message string) {
	log.WithField(from, "signal").Info(message)
}
