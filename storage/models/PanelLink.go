package models

import (
	"bitbucket.org/zsem/plan-lekcji/utils"
	"errors"
	"strings"
)

type PanelLink struct {
	Content string `json:"content"`
	Slug    string `json:"slug"`
}

func NewPanelLink(linkType string, content string) (PanelLink, error) {
	slug := ""

	switch {
	case linkType == "teachers":
		normalized := strings.ToLower(strings.Replace(utils.ReplacePolishCharacters(content), ".", "-", -1))
		slug = strings.Split(normalized, " ")[0]
	case linkType == "classes":
		slug = strings.ToLower(strings.Replace(content, " ", "", -1))
	case linkType == "rooms":
		normalized := utils.ReplacePolishCharacters(content)
		replacer := strings.NewReplacer(".", "", " ", "")
		slug = strings.ToLower(replacer.Replace(normalized))
	}

	if slug == "" {
		return PanelLink{}, errors.New("LinkPanel Construct: Slug couldn't be generated, link type " + linkType + " is not matching any")
	} else {
		return PanelLink{content, slug}, nil
	}
}
