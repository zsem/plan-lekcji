package utils

import (
	"strconv"
	"strings"
)

func CategorizeRoom(room string) string {
	roomNumber, err := strconv.Atoi(room)
	var category string

	if err == nil {
		switch {
		case Between(0, 8, roomNumber):
			category = "hades"
		case Between(9, 16, roomNumber):
			category = "parter"
		case Between(38, 44, roomNumber):
			category = "pierwsze piętro"
		case Between(44, 52, roomNumber):
			category = "drugie piętro"
		case roomNumber > 100:
			category = "pracownie w internacie"
		}
	} else {
		switch {
		case strings.HasPrefix(room, "sj"):
			category = "sale językowe"
		case strings.HasPrefix(room, "sg"):
			category = "sale gimnastyczne"
		case strings.HasPrefix(room, "pe") || strings.HasPrefix(room, "pra"):
			category = "pracownie w internacie"
		default:
			category = "inne"
		}
	}

	return category
}
