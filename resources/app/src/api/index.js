const PORT = (process.env.NODE_ENV === 'development' ? ':8088' : '')
const API_URL = (window.location.protocol + '//' + window.location.hostname + PORT + '/api/')

export const getData = () => {
  return fetch(API_URL + 'state').then(response => response.json())
}

export const getDeputies = () => {
  return fetch(API_URL + 'deputies').then(response => response.json())
}

export const getVersionHash = () => {
  return fetch(API_URL + 'check').then(response => response.json())
}

export const getAvailableRooms = ({ day, lesson }) => {
  return fetch(API_URL + 'available-rooms/' + day + '/' + lesson).then(response => response.json())
}
