package models

type Deputies struct {
	Schedules  *DeputyList `json:"schedules"`
	LastUpdate int         `json:"last_update"`
}
