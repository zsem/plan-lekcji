package parsing

import (
	"strconv"
	"strings"

	"bitbucket.org/zsem/plan-lekcji/scrapping"
	"bitbucket.org/zsem/plan-lekcji/storage/models"
	"bitbucket.org/zsem/plan-lekcji/utils"
	"github.com/PuerkitoBio/goquery"
	log "github.com/sirupsen/logrus"
)

type RoomParser struct {
	scrapper *scrapping.RoomScrapper
}

func NewRoomParser() *RoomParser {
	return &RoomParser{scrapping.NewRoomScrapper()}
}

func (parser *RoomParser) PrepareData() *models.RoomList {

	roomList := models.RoomList{}
	schedules := parser.scrapper.Scrape()

	for _, schedule := range schedules {
		room := parseRoomSchedule(schedule)
		category := utils.CategorizeRoom(room.Slug)

		if _, exists := roomList[category]; !exists {
			roomList[category] = []models.Room{}
		}

		roomList[category] = append(roomList[category], room)
	}

	log.WithField("Room Parsing", "Parse End").Infof("Generated %d rooom schedules", len(roomList))
	return &roomList
}

func parseRoomSchedule(schedule *goquery.Document) models.Room {
	var roomName, roomSlug string
	roomTitle := schedule.Find(".tytulnapis").Text()

	if len(roomTitle) > 3 {
		data := strings.SplitN(roomTitle, " ", 2)
		roomSlug = data[0]
		roomName = data[1]
	} else {
		trimmed := strings.Trim(roomTitle, " ")
		roomSlug = trimmed
		roomName = trimmed
	}

	room := models.NewRoom(roomName, roomSlug)

	schedule.Find(".tabela > tbody > tr").Each(func(i int, lesson *goquery.Selection) {
		parseRoomLesson(lesson, room)
	})

	return *room
}

func parseRoomLesson(lesson *goquery.Selection, room *models.Room) {
	lessonText := lesson.Find(".nr").Text()
	lessonID, err := strconv.Atoi(lessonText)
	days := lesson.Find(".l")

	if err != nil && lessonText != "" {
		log.WithField("Room Parser", "Room Lesson Parse").Warnf("Error while parsing lessonID to int: %s", err.Error())
		return
	}

	days.Each(func(index int, cell *goquery.Selection) {
		text := cell.Text()
		if !utils.IsEmpty(text) {
			room.Schedule[utils.IndexToDay(index)][lessonID] = []models.RoomLesson{parseRoomCell(cell)}
		}
	})
}

func parseRoomCell(cell *goquery.Selection) models.RoomLesson {
	classes := cell.Find(".o")
	classText := classes.First().Text()
	lessonName := cell.Find(".p")
	lessonText := lessonName.Text()
	teacher := cell.Find(".n")

	if classes.Length() > 1 {
		classes.Slice(1, classes.Length()).Each(func(i int, clazz *goquery.Selection) {
			classText += " " + clazz.Text()
		})
	}

	// Parse niezformatowanych przypadków
	if lessonText == "" {
		lessonText = cell.Text()
	}

	return models.NewRoomLesson(lessonText, utils.Slug(teacher.Text()), classText)
}
