package net

import (
	"strconv"

	"bitbucket.org/zsem/plan-lekcji/config"
	"bitbucket.org/zsem/plan-lekcji/storage"
	log "github.com/sirupsen/logrus"
)

func RegisterRoutes(router *Router) {

	router.Get("/api/state", func(server *Server) {
		server.Json(server.DataStorage.GetState())
	})

	router.Get("/api/check", func(server *Server) {
		server.Json(server.DataStorage.GetState().LastUpdate)
	})

	router.Get("/api/deputies", func(server *Server) {
		server.Json(server.DataStorage.GetState().Deputies)
	})

	router.Authorized("/api/update-schedule", func(server *Server) {
		log.Info("Schedule update request submitted.")
		state, err := storage.NewState()

		if err != nil {
			log.WithField("Router", "Schedule Update").Errorf("Error while preparing new state: %s", err.Error())
			server.Error(500, err.Error())
		}

		server.DataStorage.StoreAndLoad(state)
		server.Json("Zaktualizowano pomyślnie")
	})

	router.Get("/api/available-rooms/:day/:hour", func(server *Server) {
		day, derr := strconv.Atoi(server.Param("day"))
		hour, herr := strconv.Atoi(server.Param("hour"))

		if derr != nil || herr != nil {
			server.Error(400, "Bad param format")
			return
		}

		rooms := server.DataStorage.AvailableRooms(day, hour)
		server.Json(rooms)
	})

	router.ServeResources("*", showApp)
	router.Default(showApp)
}

func showApp(server *Server) {
	if config.Env.Mode == "prod" {
		server.View("index", nil)
	} else {
		server.View("app/dist/index", nil)
	}
}
