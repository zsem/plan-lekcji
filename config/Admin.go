package config

type Admin struct {
	Name     string `json:"name"`
	Password string `json:"password"`
}

func validateAdmin(admin *Admin) {
	checkField(admin.Name, "", "admin|name")
	checkField(admin.Password, "", "admin|password")
}
