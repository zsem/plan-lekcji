package models

type RoomSchedule struct {
	RoomLesson
	Timestamp
}

func NewRoomSchedule(lesson RoomLesson, start string, end string) RoomSchedule {
	return RoomSchedule{lesson, Timestamp{start, end}}
}
