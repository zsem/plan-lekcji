package models

type LessonTimeframe struct {
	Start string `json:"start"`
	End   string `json:"end"`
}

func NewLessonTimeframe() LessonTimeframe {
	return LessonTimeframe{}
}
