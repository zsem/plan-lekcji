package net

import "github.com/go-zoo/bone"

type Router struct {
	Muxer  *bone.Mux
	Server *Server
}

func NewRouter(server *Server) *Router {
	return &Router{bone.New(), server}
}

func (router *Router) Get(url string, callback RouterHandler) {
	router.Muxer.Get(url, router.Server.CreateRequestHandler(callback))
}

func (router *Router) Post(url string, callback RouterHandler) {
	router.Muxer.Post(url, router.Server.CreateRequestHandler(callback))
}

func (router *Router) Patch(url string, callback RouterHandler) {
	router.Muxer.Patch(url, router.Server.CreateRequestHandler(callback))
}

func (router *Router) Delete(url string, callback RouterHandler) {
	router.Muxer.Delete(url, router.Server.CreateRequestHandler(callback))
}

func (router *Router) Authorized(url string, handler RouterHandler) {
	router.Muxer.Post(url, router.Server.CreateAuthorizedHandler(handler))
}

func (router *Router) ServeResources(url string, fileNotFoundHandler RouterHandler) {
	router.Muxer.Get(url, router.Server.TryFile(fileNotFoundHandler))
}

func (router *Router) Default(handler RouterHandler) {
	router.Muxer.NotFound(router.Server.CreateRequestHandler(handler))
}

func (router *Router) Handler() *bone.Mux {
	return router.Muxer
}
