import Vue from 'vue'
import App from './App'
import store from './store'
import router from './router'
import VueTouch from 'vue-touch'
import Ripple from 'vue-ripple-directive'

Vue.config.productionTip = false
Vue.use(VueTouch, {name: 'v-touch'})
Vue.directive('ripple', Ripple)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
})
