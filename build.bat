@echo off
go get
go build -o bin/plan.exe
copy "env_production.json" "bin/env.json"
cd resources/app
call npm install
call npm run build
xcopy "./dist" "../../bin/app" /e /i /h /y