package utils

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"
)

type Clock struct {
	Date time.Time
}

func NewClock(hours int, minutes int) *Clock {
	t := time.Time{}
	t = t.Add(time.Hour*time.Duration(hours) + time.Minute*time.Duration(minutes))

	return &Clock{t}
}

func ClockFromString(timestring string) (*Clock, error) {
	var clock *Clock
	hours, minutes, err := GetStringTime(timestring)

	if err != nil {
		return clock, err
	}

	return NewClock(hours, minutes), nil
}

func (clock *Clock) Time() string {
	return clock.Date.Format("15:04")
}

func (clock *Clock) Add(minutes int) {
	clock.Date = clock.Date.Add(time.Minute * time.Duration(minutes))
}

func (clock *Clock) Compare(starttime string, endtime string) int {
	starth, startm, starterr := GetStringTime(starttime)
	endh, endm, enderr := GetStringTime(endtime)

	fmt.Println(endtime)

	fmt.Printf("%d %d", starth, endh)
	if starterr != nil {
		return -2
	}

	if enderr != nil {
		return -2
	}

	if starth > endh {
		return 1
	}

	if starth < endh {
		return -1
	}

	if starth == endh {
		if startm == endm {
			return 0
		}

		if startm > endm {
			return 1
		} else {
			return -1
		}
	}

	return -2
}

func GetStringTime(time string) (int, int, error) {
	times := strings.Split(time, ":")

	if len(times) != 2 {
		return 0, 0, errors.New("Invalid timestring for clock, should be in format mm:ss, passed " + time)
	}

	hours, hourErr := strconv.Atoi(times[0])
	minutes, minErr := strconv.Atoi(times[1])

	if hourErr != nil || minErr != nil {
		return 0, 0, errors.New("Time conversion failed for given string, ERR: " + hourErr.Error() + " " + minErr.Error())
	}

	return hours, minutes, nil
}

func CurrentDateTimestamp() (int, error) {
	t := time.Now()
	converted, err := strconv.Atoi(t.Format("200601021504"))

	return converted, err
}

func GetCurrentWeekdaySlug() string {
	weekday := int(time.Now().Weekday())
	var parsed int
	if weekday == 0 {
		parsed = 6
	} else {
		parsed = weekday - 1
	}

	return IndexToDay(parsed)
}

func IndexToDay(index int) string {
	days := []string{
		"mo",
		"tu",
		"we",
		"th",
		"fr",
		"sa",
		"su",
	}

	return days[index]
}
