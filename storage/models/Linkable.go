package models

type Linkable struct {
	Name string `json:"name"`
	Slug string `json:"slug"`
}
