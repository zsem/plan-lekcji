import * as _ from 'lodash'
import * as mutations from '@/store/mutations'
import actualState from '@/store/state'

describe('mutations', () => {
  it('setReady', () => {
    const state = _.clone(actualState)
    mutations.setReady(state, true)
    expect(state.ready).to.equal(true)
  })

  it('setUpdating', () => {
    const state = _.clone(actualState)
    mutations.setUpdating(state, true)
    expect(state.updating).to.equal(true)
  })

  it('setError', () => {
    const state = _.clone(actualState)
    mutations.setError(state, true)
    expect(state.error).to.equal(true)
  })

  it('setActionBarVisible', () => {
    const state = _.clone(actualState)
    mutations.setActionBarVisible(state, false)
    expect(state.error).to.equal(false)
  })

  it('sortData', () => {
    const state = _.clone(actualState)
    const sorted = _.clone(actualState)

    state.teachers = {
      b: [{ name: 'E.Bereś' }, { name: 'A.Baran' }, { name: 'M.Bochniarz' }],
      d: [{ name: 'Z.Durlak' }, { name: 'U.Dara' }, { name: 'G.Durałek' }]
    }

    state.classes = {
      1: [{ slug: '1b' }, { slug: '1c' }, { slug: '1a' }],
      2: [{ slug: '2b' }, { slug: '2c' }, { slug: '2a' }]
    }

    state.classrooms = {
      first: [{ slug: '3' }, { slug: '2' }, { slug: '1' }],
      second: [{ slug: 'sj2' }, { slug: 'sj1' }, { slug: 'sj3' }]
    }

    sorted.teachers = {
      b: [{ name: 'A.Baran' }, { name: 'E.Bereś' }, { name: 'M.Bochniarz' }],
      d: [{ name: 'U.Dara' }, { name: 'G.Durałek' }, { name: 'Z.Durlak' }]
    }

    sorted.classes = {
      1: [{ slug: '1a' }, { slug: '1b' }, { slug: '1c' }],
      2: [{ slug: '2a' }, { slug: '2b' }, { slug: '2c' }]
    }

    sorted.classrooms = {
      first: [{ slug: '1' }, { slug: '2' }, { slug: '3' }],
      second: [{ slug: 'sj1' }, { slug: 'sj2' }, { slug: 'sj3' }]
    }

    mutations.sortData(state)
    expect(state).to.deep.equal(sorted)
  })

  it('setDeputies', () => {
    const state = _.clone(actualState)
    const expected = _.clone(actualState)

    const deputies = {
      schedules: {
        mo: {
          '4i': {
            1: [
              { lesson: 1, teacher: 'Adam Nowak', class: '4i', group: 0, room: 15 }
            ]
          }
        }
      },
      last_update: 201803170020
    }

    expected.deputies = deputies
    mutations.setDeputies(state, deputies)

    expect(state).to.deep.equal(expected)
  })

  it('setData', () => {
    const state = _.clone(actualState)
    const expected = _.clone(actualState)

    const data = {
      timeframes: {
        1: {
          start: '07:10',
          end: '07:55'
        },
        2: {
          start: '08:00',
          end: '08:45'
        }
      },
      rooms: {
        first: [],
        second: []
      },
      classes: {
        1: [],
        2: []
      },
      teachers: {
        a: [],
        b: []
      },
      last_update: 1
    }

    expected.timeframes = {
      1: {
        start: '07:10',
        end: '07:55'
      },
      2: {
        start: '08:00',
        end: '08:45'
      }
    }

    expected.classrooms = {
      first: [],
      second: []
    }

    expected.classes = {
      1: [],
      2: []
    }

    expected.teachers = {
      a: [],
      b: []
    }

    expected.last_update = 1

    mutations.setData(state, data)
    expect(state).to.deep.equal(expected)
  })

  it('setHeader', () => {
    const state = _.clone(actualState)
    mutations.setHeader(state, { title: 'Test', subtitle: 'Test 2' })
    expect(state.header).to.deep.equal({
      title: 'Test',
      subtitle: 'Test 2'
    })
  })

  it('setAvailableRooms', () => {
    const state = _.clone(actualState)
    const rooms = {
      first: ['b', 'a'],
      second: ['d', 'c']
    }
    mutations.setAvailableRooms(state, rooms)
    expect(state.availableRooms).to.deep.equal({
      first: ['a', 'b'],
      second: ['c', 'd']
    })
  })

  it('setAvailableRoomsLoading', () => {
    const state = _.clone(actualState)
    mutations.setAvailableRoomsLoading(state, true)
    expect(state.availableRoomsLoading).to.equal(true)
  })

  it('setFavouriteSchedule', () => {
    const state = _.clone(actualState)
    mutations.setFavouriteSchedule(state, { type: 'test', slug: 'test2' })
    expect(state.favourite).to.deep.equal({ type: 'test', slug: 'test2' })
  })
})
