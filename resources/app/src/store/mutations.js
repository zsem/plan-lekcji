const sortScheduleData = (scheduleObject) => {
  const keys = Object.keys(scheduleObject)

  for (const key of keys) {
    scheduleObject[key].sort((a, b) => {
      const aSlug = a.slug.toLowerCase()
      const bSlug = b.slug.toLowerCase()
      if (aSlug === bSlug) return 0
      return (aSlug > bSlug ? 1 : -1)
    })
  }
}

const sortTeachers = (teachersObject) => {
  const keys = Object.keys(teachersObject)

  for (const key of keys) {
    teachersObject[key].sort((a, b) => {
      const aSurname = a.name.split('.')[1].toLowerCase()
      const bSurname = b.name.split('.')[1].toLowerCase()
      if (aSurname === bSurname) return 0
      return (aSurname > bSurname ? 1 : -1)
    })
  }
}

export const sortData = (state) => {
  sortTeachers(state.teachers)
  sortScheduleData(state.classes)
  sortScheduleData(state.classrooms)
}

export const setData = (state, data) => {
  state.classes = data.classes
  state.classrooms = data.rooms
  state.teachers = data.teachers
  state.timeframes = data.timeframes
  state.last_update = data.last_update
  sortData(state)
}

export const setDeputies = (state, deputies) => {
  state.deputies = deputies
}

export const setReady = (state, ready) => {
  state.ready = ready
}

export const setUpdating = (state, updating) => {
  state.updating = updating
}

export const setError = (state, error) => {
  state.error = error
}

export const setHeader = (state, { title = undefined, subtitle = undefined }) => {
  state.header.title = title
  state.header.subtitle = subtitle
}

export const setActionBarVisible = (state, visibility) => {
  state.actionBarVisible = visibility
}

export const setAvailableRooms = (state, rooms) => {
  const keys = Object.keys(rooms)

  for (const key of keys) {
    rooms[key].sort((a, b) => {
      const aRoom = a.toLowerCase()
      const bRoom = b.toLowerCase()
      if (aRoom === bRoom) return 0
      return (a.toLowerCase() > b.toLowerCase() ? 1 : -1)
    })
  }

  state.availableRooms = rooms
}

export const setAvailableRoomsLoading = (state, loading) => {
  state.availableRoomsLoading = loading
}

export const setFavouriteSchedule = (state, { type = '', slug = '' }) => {
  state.favourite.type = type
  state.favourite.slug = slug
}
