package models

type ClassList []Class
type TeacherList map[string][]Teacher
type RoomList map[string][]Room
type DailyDeputies map[string]map[int][]*Deputy
type DeputyList map[string]DailyDeputies
