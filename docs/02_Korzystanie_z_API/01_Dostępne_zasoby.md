Planer udostępnia publicznie następujące zasoby pod adresem zaczynającym się od *url_planera/api*: 

* Plan lekcji - */state*
* Zastępstwa - */deputies*
* Hash ostatniej aktualizacji ( jest to liczba całkowita, która rośnie wraz z czasem ostatniej aktualizacji, używana w celu sprawdzania ostatniej daty aktualizacji) - */check*
