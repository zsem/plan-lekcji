const actionsInjector = require('inject-loader!@/store/actions')
import { testAction, testActionDispatching } from '../helpers'
require('../helpers/api')

const actions = actionsInjector({
  '@/api': {
    getData () {
      return fetch('/state').then(response => response.json())
    },
    getDeputies () {
      return fetch('/deputies').then(response => response.json())
    },
    getVersionHash () {
      return fetch('/check').then(response => response.json())
    },
    getAvailableRooms ({ day, lesson }) {
      return fetch('/available-rooms/' + day + '/' + lesson).then(response => response.json())
    }
  }
})

describe('actions', () => {
  localStorage.removeItem('data')

  it('getData from network', (done) => {
    testActionDispatching(actions.getData, null, {}, [
      { type: 'getFavouriteSchedule' },
      { type: 'fetchDeputies' },
      { type: 'fetchData' }
    ], done)
  })

  it('getData from localStorage', (done) => {
    localStorage.setItem('data', '{}')

    testActionDispatching(actions.getData, null, {}, [
      { type: 'getFavouriteSchedule' },
      { type: 'fetchDeputies' },
      { type: 'setData', payload: {} },
      { type: 'checkForUpdate' }
    ], done)
  })

  it('fetchDeputies', (done) => {
    testAction(actions.fetchDeputies, null, {}, [
      {
        type: 'setDeputies',
        payload: {
          schedules: {
            mo: {
              '4i': {
                1: [
                  { lesson: 1, teacher: 'Adam Nowak', class: '4i', group: 0, room: 15 }
                ]
              }
            }
          },
          last_update: 201803170020
        }
      }
    ], done)
  })

  it('getFavouriteSchedule', (done) => {
    testAction(actions.getFavouriteSchedule, null, {}, [
      { type: 'setFavouriteSchedule', payload: {} }
    ], done)
  })

  it('fetchData', (done) => {
    const data = {
      timeframes: {
        1: {
          start: '07:10',
          end: '07:55'
        }
      },
      classes: {
        1: [
          { name: '1a automatyk', slug: '1a', schedule: {} }
        ]
      },
      teachers: {
        a: [
          { name: 'K.Adamek', slug: 'kd', schedule: {} }
        ]
      },
      rooms: {
        hades: [
          { name: 'Pracownia UTK', slug: '6', schedule: {} }
        ]
      }
    }

    testAction(actions.fetchData, null, {}, [
      { type: 'setUpdating', payload: true },
      { type: 'setUpdating', payload: false }
    ], done)

    testActionDispatching(actions.fetchData, null, {}, [
      { type: 'setData', payload: data },
      { type: 'saveData', payload: data }
    ], done)
  })

  it('setData', (done) => {
    testAction(actions.setData, {}, {}, [
      { type: 'setData', payload: {} },
      { type: 'setReady', payload: true }
    ], done)
  })

  it('checkForUpdate', (done) => {
    testActionDispatching(actions.checkForUpdate, null, { last_update: '1' }, [
      { type: 'updateData' }
    ], done)
  })

  it('updateData', (done) => {
    testActionDispatching(actions.updateData, null, {}, [
      { type: 'getData' }
    ], done)
  })

  it('setHeader', (done) => {
    const header = { title: 'Test', subtitle: 'Test 2' }
    testAction(actions.setHeader, header, {}, [
      { type: 'setHeader', payload: header }
    ], done)
  })

  it('clearHeader', (done) => {
    testActionDispatching(actions.clearHeader, null, {}, [
      { type: 'setHeader', payload: {} }
    ], done)
  })

  it('setActionBarVisible', (done) => {
    const visible = true
    testAction(actions.setActionBarVisible, visible, {}, [
      { type: 'setActionBarVisible', payload: visible }
    ], done)
  })

  it('getAvailableRooms', (done) => {
    const rooms = {
      hades: ['5', '6']
    }
    const query = { day: 0, lesson: 0 }

    testAction(actions.getAvailableRooms, query, {}, [
      { type: 'setAvailableRoomsLoading', payload: true },
      { type: 'setAvailableRooms', payload: rooms },
      { type: 'setAvailableRoomsLoading', payload: false }
    ], done)
  })

  it('setFavouriteSchedule', (done) => {
    const favourite = { type: 'clazz', slug: '4i' }

    testAction(actions.setFavouriteSchedule, favourite, {}, [
      { type: 'setFavouriteSchedule', payload: favourite }
    ], done)
  })
})
