package models

type Schedule struct {
	Lessons []ClassLesson `json:"lessons"`
	Timestamp
}

func NewSchedule(lessons []ClassLesson, start string, end string) Schedule {
	return Schedule{lessons, Timestamp{start, end}}
}
