package models

type Timestamp struct {
	Start string `json:"start"`
	End   string `json:"end"`
}
