package config

type Net struct {
	ServerPort string `json:"serverPort"`
	PublicPath string `json:"publicPath"`
	ViewPath   string `json:"viewPath"`
}

func validateNet(net *Net) {
	checkField(net.ServerPort, "", "net|serverPort")
	checkField(net.PublicPath, "", "net|publicPath")
	checkField(net.ViewPath, "", "net|viewPath")
}
