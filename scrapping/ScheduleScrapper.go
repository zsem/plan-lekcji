package scrapping

import (
	"bitbucket.org/zsem/plan-lekcji/config"
	"bitbucket.org/zsem/plan-lekcji/utils"
	"github.com/PuerkitoBio/goquery"
)

type ScheduleScrapper struct {
	Scrapper
}

func NewScheduleScrapper() ScheduleScrapper {
	baseScrapper := NewScrapper()

	return ScheduleScrapper{Scrapper: baseScrapper}
}

func (scrapper *ScheduleScrapper) Scrape() map[string][]*goquery.Selection {
	teacherPage := scrapper.GetDocument(config.Env.Endpoints.RootUrl + config.Env.Endpoints.TeacherUrl)
	schedulePage := scrapper.GetDocument(config.Env.Endpoints.RootUrl + config.Env.Endpoints.ScheduleUrl)
	roomPage := scrapper.GetDocument(config.Env.Endpoints.RootUrl + config.Env.Endpoints.RoomUrl)

	teacherLinks := utils.RetrieveLinksWithPrefix(teacherPage, "plany/n")
	scheduleLinks := utils.RetrieveLinksWithPrefix(schedulePage, "plany/o")
	roomLinks := utils.RetrieveLinksWithPrefix(roomPage, "plany/s")

	return map[string][]*goquery.Selection{
		"teachers": teacherLinks,
		"classes":  scheduleLinks,
		"rooms":    roomLinks,
	}
}
