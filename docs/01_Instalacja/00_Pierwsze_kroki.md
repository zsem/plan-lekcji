Planer dostarczany jest w formie gotowego do użytku pliku wykonywalnego, wraz z plikami konfiguracyjnymi oraz elementami interfejsu użytkownika. Dzięki temu, możemy uruchomić go w przeciagu sekund.

## Uruchomienie graficzne:

Przechodząc do katalogu z zawartym w nim planerem, uruchamiamy go poprzez start pliku wykonywalnego "plan.exe". Zostanie uruchomiona konsola pobierająca i prezentująca najnowszą wersję planu

## Uruchomienie z poziomu terminala:

Zarówno w systemie windows, jak i tych opartych na architekturze linuxa, uruchomienie następuje przez wpisanie nazwy pliku "plan.exe" lub ścieżki do niego w przypadku uruchomienia spoza jego lokalizacji. 

Dodatkowa konfiguracja, taka jak flagi aplikacji, została opisana w następnej sekcji
