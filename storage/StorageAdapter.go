package storage

type StorageAdapter interface {
	PrepareData() interface{}
	Signal(from string, message string)
}
