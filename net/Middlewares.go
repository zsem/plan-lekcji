package net

import (
	"net/http"

	"bitbucket.org/zsem/plan-lekcji/config"
)

func Cors(w http.ResponseWriter, req *http.Request) {
	if config.Env.Mode == "dev" {
		w.Header().Set("Access-Control-Allow-Origin", "*")
	}
}
