export const isReady = (state) => state.ready

export const getDeputies = (state) => state.deputies

export const isUpdating = (state) => state.updating

export const hasError = (state) => state.error

export const getTimeframes = (state) => state.timeframes

export const getClassesList = (state) => state.classes

export const getTeachersList = (state) => state.teachers

export const getClassroomsList = (state) => state.classrooms

export const getHeaderTitle = (state) => state.header.title

export const getHeaderSubtitle = (state) => state.header.subtitle

export const isActionBarVisible = (state) => state.actionBarVisible

export const getAvailableRooms = (state) => state.availableRooms

export const areAvailableRoomsLoading = (state) => state.availableRoomsLoading

export const getFavouriteSchedule = (state) => state.favourite
