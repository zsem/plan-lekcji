package parsing

import (
	"strconv"
	"strings"

	"bitbucket.org/zsem/plan-lekcji/scrapping"
	"bitbucket.org/zsem/plan-lekcji/storage/models"
	"bitbucket.org/zsem/plan-lekcji/utils"
	"github.com/PuerkitoBio/goquery"
	log "github.com/sirupsen/logrus"
)

type DeputiesParser struct {
}

func NewDeputiesParser() *DeputiesParser {
	return &DeputiesParser{}
}

func (parser *DeputiesParser) PrepareData() *models.Deputies {
	days := scrapping.NewDeputiesScrapper().Scrape()
	schedules := models.DeputyList{}
	deputies := models.Deputies{}

	for slug, documents := range days {
		schedules[slug] = parser.ParseDailyDeputies(documents)
	}

	deputies.Schedules = &schedules
	timestamp, err := utils.CurrentDateTimestamp()

	if err != nil {
		log.WithField("Deputies Parser", "Timestamp").Error("Deputies last update timestamp failed to parse:" + err.Error())
		deputies.LastUpdate = 0
	} else {
		deputies.LastUpdate = timestamp
	}

	return &deputies
}

func (parser *DeputiesParser) ParseDailyDeputies(documents []*goquery.Document) models.DailyDeputies {
	parsedData := make(models.DailyDeputies)

	if len(documents) == 0 {
		return parsedData
	}

	for _, doc := range documents {
		rows := doc.Find("table").Find("tr")

		if !(rows.Length() > 2) {
			continue
		}

		deputies := rows.Slice(2, rows.Length())

		deputies.Each(func(index int, deputy *goquery.Selection) {
			columns := deputy.Find("td")
			classAndGroup := strings.Split(utils.NodeTextContent(columns.Get(2)), "|")
			lessonNumber, lessonNumberErr := strconv.Atoi(utils.NodeTextContent(columns.Get(0)))

			if lessonNumberErr != nil {
				log.WithField("Parser", "DeputiesParser").Warn("Couldn't finish while parsing lesson number to int:" + lessonNumberErr.Error())
				return
			}

			class := utils.Slug(classAndGroup[0])
			var group string

			if len(classAndGroup) > 1 {
				groupString := classAndGroup[1]
				group = groupString[len(groupString)-3:]
			} else {
				group = ""
			}

			teacher := utils.NodeTextContent(columns.Get(5))
			room := utils.Slug(utils.NodeTextContent(columns.Get(4)))

			formattedDeputy := &models.Deputy{
				lessonNumber,
				teacher,
				class,
				group,
				room,
			}

			if parsedData[class] == nil {
				parsedData[class] = make(map[int][]*models.Deputy)
			}

			parsedData[class][lessonNumber] = append(parsedData[class][lessonNumber], formattedDeputy)
		})
	}

	return parsedData
}
