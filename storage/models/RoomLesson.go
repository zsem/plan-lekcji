package models

type RoomLesson struct {
	Lesson
	Teacher string `json:"teacher"`
	Class   string `json:"class"`
}

func NewRoomLesson(name string, teacher string, class string) RoomLesson {
	return RoomLesson{Lesson{name}, teacher, class}
}
